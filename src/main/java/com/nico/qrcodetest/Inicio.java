package com.nico.qrcodetest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.EncodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class Inicio {

	public static void main(String[] args) throws IOException, WriterException, NotFoundException {
		
		// The data that the QR code will contain
		String data = "www.geeksforgeeks.org";

		// The path where the image will be saved
		String path = "demo.png";

		// Encoding charset.
		String charset = "UTF-8";

		Map<EncodeHintType, ErrorCorrectionLevel> hashMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
		hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

		// Create the QR code and save in the specified path as a jpg file.
		MyQR.createQR(data, path, charset, hashMap, 200, 200);
		System.out.println("QR Code generated!!!");
		
		// Read the QR code.
//		String result = MyQR.readQR("demo.png", charset, hashMap);
//		System.out.println(result);
		

	}

}
