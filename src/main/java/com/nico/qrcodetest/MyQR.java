package com.nico.qrcodetest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;

public class MyQR {

	
	// Function to create the QR code
	@SuppressWarnings("rawtypes")
	public static void createQR(String data, String path,
								String charset, Map hashMap,
								int height, int width) 
		throws IOException, WriterException
	{

		
		BitMatrix matrix = new MultiFormatWriter().encode(
				new String(data.getBytes(charset), charset),
				BarcodeFormat.QR_CODE, width, height);
		
		MatrixToImageWriter.writeToPath(matrix, path.substring(path.lastIndexOf('.') + 1), Paths.get(path));
		
	}
	
	public static String readQR(String filePath, String charset, Map hintMap) throws FileNotFoundException, IOException, NotFoundException
	{
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
		        new BufferedImageLuminanceSource(
		            ImageIO.read(new FileInputStream(filePath)))));
		    Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);
		    return qrCodeResult.getText();
	}
	
	
}
